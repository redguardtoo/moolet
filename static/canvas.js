/* configuration */
var g_sceneid=null;
var g_spd_loaded={}
var g_spd='audi.spd';
var g_xml='merl__spd_index__.xml';
// Usage: if(g_mat[g_matkey]){ g_mat[g_matkey]=={diffuse:[1,2,3],specular:[1,2,3],roughness:1 }
var g_mat={};
var g_matkey='default';


function init(spd) {
if(g_spd_loaded[spd]===true)
{
	SceneJS.scene(spd).stop();
}
else{
	///loaded
  	g_spd_loaded[spd]=true;
}
SceneJS.createScene({

    /* ID that we'll access the scene by when we want to render it
     */
    id: spd,

    /* Bind scene to a WebGL canvas:
     */
    canvasId: "theCanvas",

    nodes: [
		loadSceneLibrary(),
		loadMaterialLibrary(),
        /* Viewing transform
         */
        {

	    /*
			var eye = [scene.camera[12], scene.camera[13], scene.camera[14]];
	var at = [-scene.camera[8], -scene.camera[9], -scene.camera[10]];
	var up = [scene.camera[4], scene.camera[5], scene.camera[6]];
		*/
		
            type: "lookAt",
	    id: "camera",
            eye : { x: camera[12], y:camera[13],  z: camera[14]},
	    //eye: {x: 29.3944, y: -16.4181, z: 31.8848},
            //look : { x: 0, y:0.0, z: 0 },
	    look: {x: camera[8], y: camera[9], z: camera[10]},
	    //up: {z: 1.0},
            up : {x: camera[4],y: camera[5], z: camera[6]},
	
            nodes: [

                /* Projection
                 */
                {
                    type: "camera",
                    optics: {
                        type: "perspective",
                        fovy : 25.0,
                        aspect : 1.47,
                        near : 0.10,
                        far : 3000.0
                    },

                    nodes: [

                        /* Renderer node to set BG colour
                         */
                        {
                            type: "renderer",
                            clearColor: { r: 0.435, g: 0.65, b: 0.85 },
                            clear: {
                                depth : true,
                                color : true
                            },

                            nodes: [

                                /* Point lights
                                 */
                                {
                                    type: "light",
                                    mode:                   "dir",
                                    color:                  { r: 1.0, g: 1.0, b: 1.0 },
                                    diffuse:                true,
                                    specular:               true,
                                    dir:                    { x: -1.0, y: -1.0, z: -0.5 }
                                },

                                {
                                    type: "light",
                                    mode:                   "dir",
                                    color:                  { r: 1.0, g: 1.0, b: 0.8 },
                                    diffuse:                true,
                                    specular:               false,
                                    dir:                    { x: 1.0, y: 1.0, z: 0.5 }
                                },
                                       /*-----------------------------------------------------------------------
                                         * White dot that indicates world-space ray intersection point
                                         * of each pick hit.
                                         *
                                         * We'll update its translation with the pick position on each hit.
                                         *----------------------------------------------------------------------*/
                                        {
                                            type: "name",
                                            name: "indicator",
                                            nodes: [
                                                {
                                                    type: "material",
                                                    baseColor: { r: 0, g: 1, b: 0 },
                                                    specularColor: { r: 1, g: 1, b: 1 },

                                                    nodes: [
                                                        {
                                                            type: "translate",
                                                            id: "pickIndicator",

                                                            x: -20,
                                                            z: -70,

                                                            nodes: [
                                                                {
                                                                    type: "scale",
                                                                    x: 0.5,
                                                                    y: 0.5,
                                                                    z: 0.5,
                                                                    nodes: [

                                                                        {
                                                                            type: "sphere"
                                                                        }
                                                                    ]
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        },
                                /* Modelling transforms - note the IDs, "pitch" and "yaw"
                                 */
							{
                                    type: "scale",
                                    id: "zoom",
                                    x : 1.0,
									y : 1.0,
									z : 1.0,

                                    nodes: [
									
                                {
                                    type: "rotate",
                                    id: "pitch",
                                    angle: 0.0,
                                    x : 1.0,

                                    nodes: [
                                        {
                                            type: "rotate",
                                            id: "yaw",
                                            angle: 0.0,
                                            z : 1.0,

                                            nodes: [

                                                /* Ambient, diffuse and specular surface properties
                                                 */
                                                {
                                                    type: "material",
                                                    emit: 0,
                                                    baseColor:      { r: 0.5, g: 0.5, b: 0.6 },
                                                    specularColor:  { r: 0.9, g: 0.9, b: 0.9 },
                                                    specular:       1.0,
                                                    shine:          70.0,

                                                    nodes: [
                                                            jsonScene
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
								
								] 
								} // End Scale
                            ]
                        }
                    ]
                }
            ]
        }
    ]
});


/* Get handles to some nodes
 */
var scene = SceneJS.scene(spd);
var yawNode = scene.findNode("yaw");
var pitchNode = scene.findNode("pitch");
var zoomNode = scene.findNode("zoom");
var indicatorSphere = scene.findNode("pickIndicator");

var cameraNode = scene.findNode("camera");
console.log('camera information');
console.log(cameraNode);





/* As mouse drags, we'll update the rotate nodes
 */
var lastX;
var lastY;
var dragging = false;

var newInput = false;
var yaw = 0;
var pitch = 0;
var zoom = 1;

var canvas = document.getElementById("theCanvas");

function clickCoordsWithinElement(event) {
    var coords = { x: 0, y: 0};
    if (!event) {
        event = window.event;
        coords.x = event.x;
        coords.y = event.y;
    } else {
        var element = event.target ;
        var totalOffsetLeft = 0;
        var totalOffsetTop = 0 ;

        while (element.offsetParent)
        {
            totalOffsetLeft += element.offsetLeft;
            totalOffsetTop += element.offsetTop;
            element = element.offsetParent;
        }
        coords.x = event.pageX - totalOffsetLeft;
        coords.y = event.pageY - totalOffsetTop;
    }
    return coords;
}

//not used yet - William
function updateLabelPos(pickName, canvasPos, worldPos) {

    var offset = $("#theCanvas").offset();

    $("#pickIndicator").show();
    $("#pickIndicator").html(
                    "<br/>Name: " + pickName
                    + "<br/>2d Pos: " + roundFloat(canvasPos[0]) + ", " + roundFloat(canvasPos[1])
                    + "<br/>3d Pos: " + roundFloat(worldPos[0]) + ", " + roundFloat(worldPos[1]) + ", " + roundFloat(worldPos[2])
            );

    $("#pickIndicator").css("left", offset.left + /*canvasPos[0] +*/ 5);
    $("#pickIndicator").css("top", offset.top + /*canvasPos[1] +*/ 5);
}

function roundFloat(v) {
    return Math.round(v * 10) / 10;
}

function mouseDown(event) {
	
	lastX = event.clientX;
	lastY = event.clientY;
	dragging = true;
	
	// william's pick test start
	var coords = clickCoordsWithinElement(event);

	var hit = scene.pick(coords.x, coords.y);

	if (hit) {
		
		//use MERL diffuse
		var tempDiffuse=[];
		tempDiffuse[0] = 0.0;
		tempDiffuse[1] = 0.7;
		tempDiffuse[2] = 0.0;
		if(g_matkey) {
			if(g_mat[g_matkey]) {
				var d=g_mat[g_matkey];
				tempDiffuse[0] = d.diffuse[0];
				tempDiffuse[1] = d.diffuse[1];
				tempDiffuse[2] = d.diffuse[2];
			}
		}

		//
		var materialID = hit.name + "MID";
		scene.findNode(materialID).set("baseColor", {r: tempDiffuse[0], g: tempDiffuse[1], b: tempDiffuse[2]});


		//alert("Picked 'name' node with name '" + hit.name + "' at canvasX=" + hit.canvasX + ", canvasY=" + hit.canvasY);
	} else {
		//alert("Nothing picked");
	}
	// william's pick test end

}

function mouseUp() {
    dragging = false;
}

function mouseMove(event) {
    if (dragging) {

        yaw += (event.clientX - lastX) * 0.5;
        pitch -= (event.clientY - lastY) * 0.5;
		//pitch += (event.clientY - lastY) * 0.5;
		pitch = Math.max(-90, Math.min(90, pitch));
				
        lastX = event.clientX;
        lastY = event.clientY;

        newInput = true;
    }
	else
	{
		// Fix refresh issue - William
		/*
		var coords = clickCoordsWithinElement(event);
		var hit = scene.pick(coords.x, coords.y, { rayPick: true });


		if (hit) 
		{   // Ignore hits on the indicator sphere

			if (hit.name != "indicator") 
			{
				var worldPos = hit.worldPos;

				indicatorSphere.set({
				x: worldPos[0],
				y: worldPos[1],
				z: worldPos[2]
				});

				var arrIds = hit.name.split(".");
				for(var i=0;i<jsonElements.length;i++)
				{
					if(jsonElements[i].id == arrIds[0])
					{
						$("#pickIndicator").show();
						updateLabelPos(jsonElements[i].name, hit.canvasPos, hit.worldPos);
						break;
					}
				}
			}
		} 
		else 
		{ // Nothing picked
			$("#pickIndicator").hide();
		}
		*/
	}
}

function mouseWheel(event) {
    var delta = 0;
    if (!event) event = window.event;
    if (event.wheelDelta) {
        delta = event.wheelDelta / 120;
        if (window.opera) delta = -delta;
    } else if (event.detail) {
        delta = -event.detail / 3;
    }
    if (delta) {
        if (delta < 0) {
            zoom *= 0.9;
        } else {
            zoom *= 1.1;
        }
    }
    if (event.preventDefault)
        event.preventDefault();

    newInput = true;
}

//used for pick mesh objects
function clickCoordsWithinElement(event) {
    var coords = { x: 0, y: 0};
    if (!event) {
        event = window.event;
        coords.x = event.x;
        coords.y = event.y;
    } else {
        var element = event.target ;
        var totalOffsetLeft = 0;
        var totalOffsetTop = 0 ;

        while (element.offsetParent)
        {
            totalOffsetLeft += element.offsetLeft;
            totalOffsetTop += element.offsetTop;
            element = element.offsetParent;
        }
        coords.x = event.pageX - totalOffsetLeft;
        coords.y = event.pageY - totalOffsetTop;
    }
    return coords;
}

canvas.addEventListener('mousedown', mouseDown, true);
canvas.addEventListener('mousemove', mouseMove, true);
canvas.addEventListener('mouseup', mouseUp, true);
canvas.addEventListener('mousewheel', mouseWheel, true);
canvas.addEventListener('DOMMouseScroll', mouseWheel, true);
/* Start the scene rendering
 */
scene.start({
    idleFunc: function() {
        if (newInput) {
            yawNode.set("angle", yaw);
            pitchNode.set("angle", pitch);
			zoomNode.set("x", zoom);
			zoomNode.set("y", zoom);
			zoomNode.set("z", zoom);
            newInput = false;
			
			$("#pickIndicator").hide();
        }
    }
});

}

/* UI Actions handling functions
*/
$(document).ready(function () {
 $.ajax({ type: "GET",
        url: '/load/'+g_spd,
        dataType: 'script',
        success:function(data){
		g_sceneid=g_spd;
		init(g_sceneid);
        },
        error: function(data){
	alert('falied to download '+g_spd);
        }
    });

 $.ajax({ type: "GET",
        url: '/static/'+g_xml,
        dataType: 'script',
        success:function(xml){
	$(xml).find('spd').each(function(){
		$(this).find('MeasuredMaterial').each(function(){
			var k=$(this).attr('name');
			var d=eval('['+$(this).find('diffuse').text().replace(/ /g,',')+']');
			var s=eval('['+$(this).find('specular').text().replace(/ /g,',')+']');
			var r=eval($(this).find('roughness').text());
			g_mat[k]={
				diffuse:d,
				specular:s,
				roughness:r
			};
			$('#mat').append($('<option>', { k : k }).text(k)); 
			console.log($(this).attr('name'));
		});
		});
        },
        error: function(data){
	alert('falied to download '+g_xml);
        }
    });
 

$('#spd').change(function(){
	g_spd=$('#spd').val();
	$.ajax({ type: "GET",
		url: '/load/'+g_spd,
		dataType: 'script',
		success:function(data){
		console.log(g_spd+' successfully downloaded!');
		g_sceneid=g_spd+(new Date()).getTime();
		init(g_sceneid);
		},
error: function(data){
	alert('falied to download '+g_spd);
}
});
	});

$('#mat').change(function(){
	g_matkey=$('#mat').val();
	if(g_mat[g_matkey]){ 
		var d=g_mat[g_matkey];
		var msg='';
		msg+='diffuse: '+d.diffuse[0]+' '+d.diffuse[1]+' '+d.diffuse[2]+'\n'; 
		msg+='specular: '+d.specular[0]+' '+d.specular[1]+' '+d.specular[2]+'\n'; 
		msg+='roughness: '+d.roughness+'\n';
		$('#mat_details').html(msg);
	}
	
});

$('#submitjob').click(function(){
	var arrstr="1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16"; /* Change the code HERE*/
	console.log('submitjob clicked');
	$('#submitjob').attr('disabled', true);
	$.ajax({
            type: "POST",
            url: '/save',
            dataType:"json",
            data:{"spd":g_spd,"data":arrstr},
            success:function(data){
		alert('submit succeeded!');
	    	$('#submitjob').attr('disabled', false);
            },
            error: function(data){
		alert('submit failed for network problem');
	    	$('#submitjob').attr('disabled', false);
                   }
        });

});

});
