#!/usr/bin/env python

import os, sys, json, inspect
import web

cmd_folder = os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0])
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

import moonlet64 #moonlet64.so must exist in sys.path

#USER CONFIGURATION
SPD_DIR=os.getcwd()+"/static" #current working directory by default
JSON_DIR=os.getcwd()


#initlization
render = web.template.render('templates/')

sys.path.append(os.path.dirname(__file__))

urls = (
    r'/','Index',
    r'/load/(.+)','Load',
    r'/save','Save',
    r'/test','Home'
)

class Index:
    def GET(self):
        return render.index()

class Load:
    def GET(self,spd):
        camera='default'
        global SPD_DIR 
        spd_path=SPD_DIR+"/"+spd
        moon = moonlet64.SceneProcess()
        result = moon.loadPackage(str(spd_path), str(camera))
        jsonFile=str(JSON_DIR+"/"+spd+camera+".json")
        if not os.path.exists(jsonFile):
            moon.writeToSceneJS(jsonFile)
        resp=open(jsonFile, 'r').read()
        web.header('Content-Type', 'text/plain')
        return resp
        #return "spd:{0} and camera:{1} {2}".format(spd_path,camera,SPD_DIR)

# paste below code into index.html:
#<form method="post" action="save">
#<p><input type="text" name="spd" /> blah blah ....<input type="submit" value="Save" /></p>
#</form>
class Save:
    def POST(self):
        i=web.input()
        # i.spd='cornell.spd' i.date="1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6"
        spd=i.spd
        arrstr=i.data
        print "spd={0} arrstr={1}".format(spd,arrstr)
        return '[0]'

class Home(object):
    def GET(self):
        return "Hello World, Bye World"

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()

# vim: set expandtab tabstop=4 shiftwidth=4:
